# Free Flying Plane Unity

- Game rules
    - control the plane to fly as far as possible
    - score will be increased with the flying distance
    - getting the coins and gain 1000 score
    - colliding with the obstacle including the sky will lose one life
    - there are total 5 lives each round
    - game will be over if the plane loses all its lives or touches the ground
- Game control
    - press keyboard "Space" button to add an upper force to the plane 
    - game will be restarted if you press the retry button

 <img src="./FreeFlyingPlane-demo.gif" width="500" height="500" />

 ------
 Project written in 2022 by Cindy Tang
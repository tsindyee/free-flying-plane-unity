using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    ObjectManager manager;
    // Start is called before the first frame update
    void Start()
    {
        manager = transform.parent.GetComponent<ObjectManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-manager.movingSpeed * Time.deltaTime, 0, 0);
        // Debug.Log(manager.movingSpeed);
        if (transform.position.x < -6f) {
            Destroy(gameObject);
        }
    }
}

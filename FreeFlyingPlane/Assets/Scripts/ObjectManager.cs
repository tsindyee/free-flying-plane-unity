using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : MonoBehaviour
{
    [SerializeField] GameObject[] WallPrefabs;

    float createTime;

    public float movingSpeed = 1.5f;
    public void generateNewWall() {
        int r = Random.Range(0, WallPrefabs.Length);
        GameObject wall = Instantiate(WallPrefabs[r], transform);
        wall.transform.position = new Vector3(7, Random.Range(-4f, 3f), 0);
    }

    private void Update() {
        createTime += Time.deltaTime;
        if (createTime > 1f) {
            createTime = 0f;
            generateNewWall();
        }
        movingSpeed += 0.05f *Time.deltaTime;
    }
}

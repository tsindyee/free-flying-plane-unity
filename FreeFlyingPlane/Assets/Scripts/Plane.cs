using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Plane : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody2D rigidBody;
    [SerializeField] public float buttonTime = 1f;
    float jumpTime;
    [SerializeField] public float jumpAmount = 0.5f;
    [SerializeField] Text scoreText;
    [SerializeField] GameObject HpBar;
    [SerializeField] int Hp;
    [SerializeField] GameObject replayButton;
    int score;
    float scoreTime;
    bool jumping;
    Animator animator;
    void Start()
    {
        Hp = 5;
        updateHpBar();
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        replayButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            jumping = true;
            jumpTime = 0;
        }
        if (jumping)
        {
            // rigidBody.velocity = new Vector2(0, jumpAmount * Time.deltaTime);
            rigidBody.AddForce(new Vector2(0, jumpAmount * Time.deltaTime), ForceMode2D.Impulse);
            jumpTime += Time.deltaTime;
        }
        if (Input.GetKeyUp(KeyCode.Space) | jumpTime > buttonTime)
        {
            jumping = false;
        }
        updateScore();
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "Sky") {
            Debug.Log("Trigger Sky");
            animator.SetTrigger("hurt");
            modifyHp(-1);
        } else if (other.gameObject.tag == "Ground") {
            Debug.Log("Trigger Ground");
            modifyHp(-5);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Wall") {
            Debug.Log("Trigger Wall");
            animator.SetTrigger("hurt");
            modifyHp(-1);
        } else if (other.gameObject.tag == "Coin") {
            Debug.Log("Trigger Coin");
            score += 1000;
            Destroy(other.gameObject);
            updateScoreText(score);
        } 
    }

    void updateScoreText(int score) {
        scoreText.text = "Score: " + score.ToString();
    }
    void updateScore() {
        scoreTime += Time.deltaTime;
        if (scoreTime > 2f) {
            score += 100;
            scoreTime = 0f;
            updateScoreText(score);
        }
    }
        void modifyHp(int num) {
        Hp += num;
        if (Hp <= 0) {
            Hp = 0;
            gameOver();
        }
        updateHpBar();
    }
    void updateHpBar() {
        for (int i = 0; i < HpBar.transform.childCount; i++) {
            if (Hp > i) {
                HpBar.transform.GetChild(i).gameObject.SetActive(true);
            } else {
                HpBar.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
    void gameOver() {
        Time.timeScale = 0;
        replayButton.SetActive(true);
    }

    public void Replay() {
        Time.timeScale = 1f;
        replayButton.SetActive(false);
        Hp = 5;
        score = 0;
        scoreTime = 0f;
        updateHpBar();
        SceneManager.LoadScene("SampleScene");
    }
}
